import { Component,OnInit } from '@angular/core';
import { TripService } from '../trip.service';
import { UserService } from '../user.service';
import { switchMap } from 'rxjs';

@Component({
  selector: 'app-mybookings',
  templateUrl: './mybookings.component.html',
  styleUrls: ['./mybookings.component.css']
})
export class MybookingsComponent implements OnInit{

  profileId:any;
  bookedTrips:any;

  constructor(private tripservice:TripService,private userservice:UserService){
    this.bookedTrips={}
  }

  // ngOnInit(): void {
  //   this.userservice.getProfileId().pipe(switchMap((userId: any) => {
  //       this.userId = userId;
  //       console.log(this.userId);
  //       return this.tripservice.getBookedTripsByUser(this.userId);
  //     }) ).subscribe((bookedTrips: any) => {
  //     this.bookedTrips = bookedTrips;
  //   });
  // }

  ngOnInit(): void {
    this.profileId=this.userservice.getLoggedProfileId();
    this.tripservice.getBookedTripsByUser(this.profileId).subscribe((bookedTrips: any) => {
      this.bookedTrips = bookedTrips;
     });
    }  

   
    // this.userservice.getProfileId().subscribe((userId:any)=>{
    //   this.userId=userId;
    //   console.log("inside"+userId)
    // })
    // console.log("outside"+this.userId)
    // this.tripservice.getBookedTripsByUser(this.userId).subscribe((bookedTrips:any)=>{
    //   this.bookedTrips=bookedTrips;
    // })
  

}
