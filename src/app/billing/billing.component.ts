import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TripService } from '../trip.service';

@Component({
  selector: 'app-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.css']
})
export class BillingComponent implements OnInit{

  tripId: any;
  trip:any;
  user:any;
  billingTrip:any;

  constructor( private route: ActivatedRoute,
    private service:UserService, private tripservice: TripService, private router : Router ) {
    this.billingTrip = {
    }
    
  }

  ngOnInit(): void {
    // this.service.getTripId().subscribe((tripId:any)=>{
    //   this.tripId=tripId;
    // })
    this.route.params.subscribe(params => {
      this.tripId = params['billingId'];
      console.log(this.tripId)
    });
    const profileId=this.service.getLoggedProfileId();

    this.user=this.service.getUserById(profileId);
    console.log(this.user);


    this.tripservice.getTripById(this.tripId).subscribe((trip: any) => {
      this.trip = trip;
      this.billingTrip= { 
        heroImage:this.trip.heroImage, 
        tripTitle: this.trip.tripTitle,
        continent: this.trip.continent,
        country: this.trip.country,
        tripType: this.trip.tripType,
        price: this.trip.price,
        activityLevel: this.trip.activityLevel,
        serviceLevel: this.trip.serviceLevel,
        tripDuration: this.trip.tripDuration,
        tripOverview: this.trip.tripOverview,
        itinerary: this.trip.itinerary,
        whatToExpect: this.trip.whatToExpect,
        accommodation:this.trip.accommodation,
        expertTeam: this.trip.expertTeam,
        transportation: this.trip.transportation,
        whatIncluded: this.trip.whatIncluded,
        whatNotIncluded: this.trip.whatNotIncluded
      }
    }, (error: any) => {
      console.error(error);
    });
  }

  razorPay(){
    this.router.navigate(['/payment']);
  }

}
