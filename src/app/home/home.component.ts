import { Component, OnInit } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { EdittripComponent } from '../edittrip/edittrip.component';
import { ToastrService } from 'ngx-toastr';
import { TripService } from '../trip.service';

declare const $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  trips: any[] = [];
  // userId: any;
  favoriteTrips: any;



  profileId: any;

  currentTrips: any[] = [];
  selectedCountries: string[] = [];
  selectedContinents: string[] = [];
  selectedDuration: string = '';
  selectedPrice: string = '';
  priceSort: string = 'asc';
  durationSort: string = 'asc';


  loginStatus: any;
  // isFavorite:any;
  adminLoginStatus: any;



  selectCountries = [
    { value: '', label: 'All' },
    { value: 'INDIA', label: 'INDIA' },
    { value: 'BORNEO', label: 'BORNEO' },
    { value: 'CAMBODIA', label: 'CAMBODIA' },
    { value: 'MONGOLIA', label: 'MONGOLIA' },
    { value: 'JAPAN', label: 'JAPAN' },
    { value: 'TANZANIA', label: 'TANZANIA' },
    { value: 'KENYA', label: 'KENYA' },
    { value: 'TANZANIA', label: 'TANZANIA' },
    { value: 'MADAGASCAR', label: 'MADAGASCAR' },
    { value: 'BOTSWANA', label: 'BOTSWANA' },
    { value: 'NAMIBIA', label: 'NAMIBIA' },
    { value: 'SOUTH AFRICA', label: 'SOUTH AFRICA' },
    { value: 'UGANDA', label: 'UGANDA' },
    { value: 'ZAMBIA', label: 'ZAMBIA' },
    { value: 'ZIMBABWE', label: 'ZIMBABWE' },
    { value: 'AUSTRALIA', label: 'AUSTRALIA' },
    { value: 'NEW ZEALAND', label: 'NEW ZEALAND' },
    { value: 'PALAU', label: 'PALAU' }
  ]
  selectContinents = [
    { value: '', label: 'All' },
    { value: 'ASIA', label: 'ASIA' },
    { value: 'AFRICA', label: 'AFRICA' },
    { value: ' Australia and the Pacific', label: ' Australia and the Pacific' }
  ]
  priceLevel = [
    { value: '', label: 'any' },
    { value: '0-1000', label: '$ 0-1000' },
    { value: '1001-10000', label: '$1001-$10000' },
    { value: '1001-10000', label: '$10001-$50000' },
    { value: '50001-100000', label: '$50001-$100000' },
    { value: '100001-1000000', label: '$100001-$1000000' },
    { value: '', label: '' }
  ]



  constructor(
    private service: UserService,
    private tripservice: TripService,
    private router: Router,
    private dialog: MatDialog,
    private toastr: ToastrService
  ) {
    this.trips = [
    ];
    // this.loginStatus=this.service.getUserLoggedStatus();
  }

  ngOnInit(): void {
    const heroSection = document.querySelector('.hero');
    if (heroSection !== null) {
      heroSection.classList.add('animate-background');
    }

    // this.loggedStatuses();
    this.loginStatus=this.service.getUserLoggedStatus();
    this.adminLoginStatus=this.service.getAdminLoggedStatus();
    // .subscribe((loginStatusData: any) => {
      // this.loginStatus = loginStatusData;
      // console.log("loginStatus"+this.loginStatus);
    // });

    this.service.getAdminLoginStatus().subscribe((adminLoginStatusData: any) => {
      this.adminLoginStatus = adminLoginStatusData;
      console.log("AdminLoginStatus"+this.adminLoginStatus);
    });
    this.getAllTrips();

    // this.userId = this.service.getProfileId();
    
    this.profileId = this.service.getLoggedProfileId();
    console.log("profileId"+this.profileId);

      this.tripservice.getUserFavoriteTrips(this.profileId).subscribe((favTrips: any) => {
        this.favoriteTrips = favTrips;
        console.log("favTrips"+this.favoriteTrips);
      })
      console.log("login : "+this.loginStatus);

  }


  // loggedStatuses() {
  //   this.service.getLoginStatus().subscribe((loginStatusData: any) => {
  //     this.loginStatus = loginStatusData;
  //     console.log(this.loginStatus);
  //   });

  //   this.service.getAdminLoginStatus().subscribe((adminLoginStatusData: any) => {
  //     this.adminLoginStatus = adminLoginStatusData;
  //   });
  // }


  getAllTrips(): void {
    this.tripservice.getAllTrips().subscribe((trips: any) => {
      this.trips = trips;
      this.applyFilters();
    });
  }

  exploreTrip(tripId: any): void {
    if(this.loginStatus || this.adminLoginStatus){
      this.router.navigate(['/tripdescription', tripId]);
    }else{
      this.router.navigate(['/login']);
    }
    
  }

  bookTrip(bookingTripId: any) {
    // const profileId=this.service.getProfileId();
    // this.service.addBookedTrip(profileId,bookingTripId);
    if(this.loginStatus){
      this.router.navigate(['/bookingtrip', bookingTripId]);
    }else{
      this.router.navigate(['/login']);
    }
    
  }

  applyFilters(): void {
    let currentTrips = this.trips;

    if (this.selectedContinents.length > 0) {
      currentTrips = currentTrips.filter(trip => this.selectedContinents.includes(trip.continent));
    }

    if (this.selectedCountries.length > 0) {
      currentTrips = currentTrips.filter(trip => this.selectedCountries.includes(trip.country));
    }

    if (this.selectedDuration) {
      const [minDuration, maxDuration] = this.selectedDuration.split('-');
      currentTrips = currentTrips.filter(trip => trip.tripDuration >= +minDuration && trip.tripDuration <= +maxDuration);
    }
    if (this.selectedPrice) {
      const [minPrice, maxPrice] = this.selectedPrice.split('-');
      currentTrips = currentTrips.filter(trip => trip.price >= +minPrice && trip.price <= +maxPrice);
    }
    if (this.priceSort === 'asc') {
      currentTrips.sort((a, b) => a.price - b.price);
    } else if (this.priceSort === 'desc') {
      currentTrips.sort((a, b) => b.price - a.price);
    }

    // Sort by duration
    if (this.durationSort === 'asc') {
      currentTrips.sort((a, b) => a.tripDuration - b.tripDuration);
    } else if (this.durationSort === 'desc') {
      currentTrips.sort((a, b) => b.tripDuration - a.tripDuration);
    }
    else {
      this.currentTrips = this.trips;
    }

    this.currentTrips = currentTrips;
  }

  // editTrip(trip: any) {
  //   const dialogRef = this.dialog.open(EdittripComponent, {
  //     width: '500px',
  //     data: { ...trip },
  //   });

  //   dialogRef.afterClosed().subscribe((updatedTrip: any) => {
  //     if (updatedTrip) {
  //       // Find the index of the updated trip in the trips array
  //       const index = this.trips.findIndex((t: any) => t.tripId === updatedTrip.tripId);

  //       if (index !== -1) {
  //         // Update the trip object in the array with the updated data
  //         this.trips[index] = { ...updatedTrip };
  //       }
  //     }
  //   });
  // }

  // In your component class
  displayLimit: number = 8;

  showMore() {
    this.displayLimit += 12; // Increase the display limit by 10 when "Show More" is clicked
  }

  // isFavorite(tripId: any): boolean {
  //   return this.favoriteTrips.some((favoriteTrip: any) => favoriteTrip.tripId === tripId);
  // }
  isFavorite(tripId: any): boolean {
    const fav=this.favoriteTrips
    return fav.findIndex((f: any) => f.tripId === tripId) !== -1;
  }

  toggleFavorite(tripId: any) {
    // trip.favorite = !trip.favorite;

    if(this.loginStatus){
      if (this.isFavorite(tripId)) {
        this.tripservice.removeFavoriteTrip(tripId, this.profileId).subscribe((result:any)=>{
          this.toastr.error(result);
        })
        console.log(tripId);
        console.log(this.profileId);
        this.toastr.error('removed from Favorites !');
      } else {
        this.tripservice.addFavoriteTrip(tripId, this.profileId).subscribe((result:any)=>{
          this.toastr.success(result);
        });
        console.log(tripId);
        console.log(this.profileId);
        this.toastr.success('added to Favorites !');
      }
    }else{
      this.router.navigate(['/login']);
    }

    
  }

}