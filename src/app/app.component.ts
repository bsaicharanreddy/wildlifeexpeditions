import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'wildLifeExpeditions';
  // backgroundImage = 'src/assets/Hero-images/hero1.jpg';

  ngOnInit(): void {
    
  }
} 
