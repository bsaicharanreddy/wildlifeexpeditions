import { Component } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { TripService } from '../trip.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-addtrip',
  templateUrl: './addtrip.component.html',
  styleUrls: ['./addtrip.component.css']
})
export class AddtripComponent {


  continents=[
    {value:'ASIA',label:'ASIA'},
    {value:'AFRICA',label:'AFRICA'},
    {value:' Australia and the Pacific',label:' Australia and the Pacific'}
  ]

  countries=[
    {value:'INDIA',label:'INDIA'},
    {value:'BORNEO',label:'BORNEO'},
    {value:'CAMBODIA',label:'CAMBODIA'},
    {value:'MONGOLIA',label:'MONGOLIA'},
    {value:'JAPAN',label:'JAPAN'},
    {value:'TANZANIA',label:'TANZANIA'},
    {value:'KENYA',label:'KENYA'},
    {value:'TANZANIA',label:'TANZANIA'},
    {value:'MADAGASCAR',label:'MADAGASCAR'},
    {value:'BOTSWANA',label:'BOTSWANA'},
    {value:'NAMIBIA',label:'NAMIBIA'},
    {value:'SOUTH AFRICA',label:'SOUTH AFRICA'},
    {value:'UGANDA',label:'UGANDA'},
    {value:'ZAMBIA',label:'ZAMBIA'},
    {value:'ZIMBABWE',label:'ZIMBABWE'},
    {value:'AUSTRALIA',label:'AUSTRALIA'},
    {value:'NEW ZEALAND',label:'NEW ZEALAND'},
    {value:'PALAU',label:'PALAU'}
  ]

  trip:any;

  constructor(private service:UserService,private router:Router,
     private tripservice:TripService,private toastr: ToastrService) {

    this.trip = {continent:'',country:'', tripType:'', imageUrl:'',badge:'',
    price:0,activityLevel:'',serviceLevel:'',tripDuration:0,tripTitle:'',heroImage:'',tripOverView:'',itenary:'',
    whatToExpect:'',accomodation:'',expertTeam:'',transportation:'',whatIncluded:'',whatNotIncluded:'',};

  }

  tripRegistration(regFrom: any) {
    // console.log(regFrom);

    this.trip.continent = regFrom.continent;
    this.trip.country = regFrom.country;
    this.trip.tripType = regFrom.tripType;
    this.trip.imageUrl = regFrom.imageUrl;
    this.trip.badge = regFrom.badge;
    this.trip.price = regFrom.price;
    this.trip.activityLevel = regFrom.activityLevel;
    this.trip.serviceLevel = regFrom.serviceLevel;
    this.trip.tripDuration = regFrom.tripDuration;
    this.trip.heroImage = regFrom.heroImage;
    this.trip.tripOverView = regFrom.tripOverView;
    this.trip.itenary = regFrom.itenary;
    this.trip.whatToExpect = regFrom.whatToExpect;
    this.trip.accomodation = regFrom.accomodation;
    this.trip.expertTeam = regFrom.expertTeam;
    this.trip.transportation = regFrom.transportation;
    this.trip.whatIncluded = regFrom.whatIncluded;
    this.trip.whatNotIncluded = regFrom.whatNotIncluded;


    console.log(this.trip);
    this.tripservice.registerTrip(this.trip).subscribe((data: any) => {
      console.log(data);
      
    });
    this.toastr.success('trip added successful !');
    // this.router.navigate(['trip']);
  }

}
