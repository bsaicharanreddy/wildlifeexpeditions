import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { TripService } from '../trip.service';

@Component({
  selector: 'app-tripdescription',
  templateUrl: './tripdescription.component.html',
  styleUrls: ['./tripdescription.component.css']
})
export class TripDescriptionComponent implements OnInit {
 
  tripId: any;
  trip:any;
  tripDesc:any;

  constructor( private route: ActivatedRoute, private service: UserService, private tripService: TripService ) {
    this.tripDesc = {  
      tripTitle: '',
      continent: '',
      country: '',
      tripType: '',
      price: '',
      activityLevel: '',
      serviceLevel: '',
      tripDuration: '',
      tripOverview: ''
    }
    
  }

  ngOnInit(): void {
    // this.service.getTripId().subscribe((tripId:any)=>{
    //   this.tripId=tripId;
    // })
    this.route.params.subscribe(params => {
      this.tripId = params['tripId'];
    });

    this.tripService.getTripById(this.tripId).subscribe((trip: any) => {
      this.trip = trip;
      this.tripDesc= {  
        tripTitle: this.trip.tripTitle,
        continent: this.trip.continent,
        country: this.trip.country,
        tripType: this.trip.tripType,
        price: this.trip.price,
        activityLevel: this.trip.activityLevel,
        serviceLevel: this.trip.serviceLevel,
        tripDuration: this.trip.tripDuration,
        tripOverview: this.trip.tripOverview,
        itinerary: this.trip.itinerary,
        whatToExpect: this.trip.whatToExpect,
        accommodation:this.trip.accommodation,
        expertTeam: this.trip.expertTeam,
        transportation: this.trip.transportation,
        whatIncluded: this.trip.whatIncluded,
        whatNotIncluded: this.trip.whatNotIncluded
      }
    }, (error: any) => {
      console.error(error);
    });
  }

}






// import { Component, OnInit } from '@angular/core';
// import { UserService } from '../user.service';

// @Component({
//   selector: 'app-tripdescription',
//   templateUrl: './tripdescription.component.html',
//   styleUrls: ['./tripdescription.component.css']
// })
// export class TripDescriptionComponent implements OnInit{

//   trips:any[]=[];
//   tripDesc:any;
//   tripId:any;

//   constructor(private service:UserService){

//   }


//   ngOnInit(): void {

//     this.service.getAllTrips().subscribe((trip:any[])=>{
//       this.trips=trip;
//     })
//     alert('trips')

//     this.service.getTripId().subscribe((trip:any)=>{
//       this.tripId=trip;
//     })

//     const index = this.trips.findIndex((t: any) => t.tripId === this.tripId);

//       this.tripDesc =this.trips[index];
      
//   }

      

// }
  

