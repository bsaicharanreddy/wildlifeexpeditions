import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TripService } from '../trip.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  profileId:any;

user: any;
adminLoginStatus:boolean=false;

emailId: string = '';
emailTouched: boolean = false;
emailValidations: { [key: string]: boolean } = {
  format: false
};

password: string = '';
passwordTouched: boolean = false;
passwordValidations: { [key: string]: boolean } = {
  length: false,
  uppercase: false,
  lowercase: false,
  digit: false,
  special: false
};
  
  constructor(private service:UserService, private tripservice:TripService, private router:Router, private toastr: ToastrService) {
    
  }

  ngOnInit(){
     }


  getAllUsers(){
    this.service.getAllUsers().subscribe((userData:any)=>{
      this.user=userData;
    });
    return this.user;
  }


  async login(loginForm: any) {
    console.log(loginForm);   


    if (loginForm.emailId === 'Admin@gmail.com' && loginForm.password === 'Admin@123') {
      
      this.service.setAdminLoggedIn();
      this.toastr.success('Login successful !');
      this.router.navigate(['home']); 
    } else {
      await this.service.getUser(loginForm).then((userData: any) => {
        this.user = userData;
        if (this.user && this.user.userId) {
          this.profileId=this.user.userId;  
        } else {
         console.log("the user or userId is null");
        }
        
        
        console.log(userData);
      });      
      if (this.user != null) {
        
        this.service.setProfileId(this.profileId);
        this.service.setUserLoggedIn();
        this.toastr.success('Login successful !');
        this.router.navigate(['home']);
        
      } else {
        
        this.router.navigate(['login']);
        this.toastr.error('Invalid credentials');
      }   
  }
}

isEmailValid(condition: string): boolean {
  return this.emailValidations[condition];
}

clearEmailValidation(): void {
  this.emailTouched = true;
}

updateEmailValidation(): void {
  this.emailValidations['format'] = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(this.emailId);
}

//Password Validation
isPasswordValid(condition: string): boolean {
  return this.passwordValidations[condition];
}

clearPasswordValidation(): void {
  this.passwordTouched = true;
}

updatePasswordValidation(): void {
  this.passwordValidations['length'] = this.password.length >= 8;
  this.passwordValidations['uppercase'] = /[A-Z]/.test(this.password);
  this.passwordValidations['lowercase'] = /[a-z]/.test(this.password);
  this.passwordValidations['digit'] = /\d/.test(this.password);
  this.passwordValidations['special'] = /\W/.test(this.password);
}

//button validation
allConditionsSatisfied(): boolean {
  return this.isEmailValid('format') && this.isPasswordValid('length') && this.isPasswordValid('uppercase') &&
    this.isPasswordValid('lowercase') && this.isPasswordValid('digit') && this.isPasswordValid('special');
}



}