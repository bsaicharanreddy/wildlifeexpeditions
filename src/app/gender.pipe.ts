import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'gender'
})
export class GenderPipe implements PipeTransform {

  suffix:any;

  transform(name : any, gender :any): any {

    if(gender==="Male"){
      this.suffix = "Mr.";
    }else if(gender==="Female"){  
      this.suffix="Miss.";
    }else {
      this.suffix="";
    }
    name  = ""+this.suffix+name;
  
    return name;
  }

}