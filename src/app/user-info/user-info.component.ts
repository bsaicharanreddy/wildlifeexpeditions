import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { EdituserComponent } from '../edituser/edituser.component';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

//Declare the jQuery variable at the top.
declare var jQuery: any;



@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css']
})
export class  UserInfoComponent implements OnInit {

    users: any;
    userObj: any;
    countries: any;
    loginStatus:any;
    adminLoginStatus:any;
    loading: boolean = false;



  constructor(
    private service: UserService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.service.getAllUsers().subscribe((userData: any) => {
      this.users = userData;
    });
    this.service.getLoginStatus().subscribe((loginStatusData: any) => {
      this.loginStatus = loginStatusData;
    });
    this.service.getAdminLoginStatus().subscribe((adminLoginStatusData: any) => {
      this.adminLoginStatus = adminLoginStatusData;
    });
    this.service.getAllCountries().subscribe((countriesData: any) => {this.countries = countriesData;});
    
  }
  
  
    deleteUser(userId: any) {
    this.service.deleteUser(userId).subscribe((data: any) => {
      console.log(data);
    });

    const i = this.users.findIndex((user: any) => {
      return user.userId == userId;
    });

    this.users.splice(i, 1);

  }

  editUser(user: any) {
    const dialogRef = this.dialog.open(EdituserComponent, {
      width: '500px',
      data: { ...user,defaultCountry: user.country },
    });
    
    dialogRef.afterClosed().subscribe((updateduser: any) => {
      if (updateduser) {
        this.service.updateUser(updateduser).subscribe((data: any) => {
          console.log(data);
          
          // Update the user object in the array with the updated data
          const index = this.users.findIndex((u: any) => u.userId === updateduser.userId);
          if (index !== -1) {
            this.users[index] = { ...updateduser };
            
          }
          this.loading = false;
        });
      }
      
        this.loading = false;
    });
  }
  

  // editUser(user: any) {
  //   const dialogRef = this.dialog.open(EdituserComponent, {
  //     width: '500px',
  //     data: { ...user },
  //   });
  
  //   dialogRef.afterClosed().subscribe((updateduser: any) => {
  //     if (updateduser) {
  //       this.service.updateUser(updateduser).subscribe((data: any) => {
  //         console.log(data);
          
  //         // Find the index of the updated user in the users array
  //         const index = this.users.findIndex((u: any) => u.userId === updateduser.userId);
    
  //         if (index !== -1) {
  //           // Update the user object in the array with the updated data
  //           this.users[index] = { ...updateduser };
  //         }
  //       });
  //     }
  //   });
  // }
  

  // editUser(user: any) {
  //   const dialogRef = this.dialog.open(EdituserComponent, {
  //     width: '500px',
  //     data: { ...user },
  //   });

  //   dialogRef.afterClosed().subscribe((updateduser: any) => {
  //     if (updateduser) {
  //       // Find the index of the updated trip in the trips array
  //       const index = this.users.findIndex((t: any) => t.tripId === updateduser.userId);

  //       if (index !== -1) {
  //         // Update the trip object in the array with the updated data
  //         this.users[index] = { ...updateduser };
  //       }
  //     }
  //   });
  // }


}


// implements OnInit {
  
//   users: any;
//   userObj: any;
//   countries: any;

//   constructor(private service: UserService) {

//     this.userObj = {userId:'', userName:'',gender:'', country:'', mobileNumber:'', emailId:'', password:''};
  
//   }

//   ngOnInit(){
//     this.service.getAllUsers().subscribe((userData: any) => { this.users = userData;});
//     this.service.getAllCountries().subscribe((countriesData: any) => {this.countries = countriesData;});
//   }

//   deleteUser(userId: any) {
//     this.service.deleteUser(userId).subscribe((data: any) => {
//       console.log(data);
//     });

//     const i = this.users.findIndex((user: any) => {
//       return user.userId == userId;
//     });

//     this.users.splice(i, 1);

//   }


  

// }

