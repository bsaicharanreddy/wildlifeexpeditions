import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AdminComponent } from './admin/admin.component';
import { LogoutComponent } from './logout/logout.component';
import { AboutComponent } from './about/about.component';
import { CountryCodePipe } from './country-code.pipe';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { UserInfoComponent } from './user-info/user-info.component';
import { GenderPipe } from './gender.pipe';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { TrialComponent } from './trial/trial.component';
import { TripComponent } from './trip/trip.component';
import { TripDescriptionComponent } from './tripdescription/tripdescription.component';
import { EdittripComponent } from './edittrip/edittrip.component';
import { MatDialogModule } from '@angular/material/dialog';
import { EdituserComponent } from './edituser/edituser.component';
import { RegistertripComponent } from './registertrip/registertrip.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { GoogleauthComponent } from './googleauth/googleauth.component';
import { BookingtripComponent } from './bookingtrip/bookingtrip.component';
import { BillingComponent } from './billing/billing.component';
import { BookingsComponent } from './bookings/bookings.component';
import { FavoritetripComponent } from './favoritetrip/favoritetrip.component';
import { MybookingsComponent } from './mybookings/mybookings.component';
import { UserProfileComponent } from './profile/profile.component';
import { AddtripComponent } from './addtrip/addtrip.component';
import { PaymentserviceComponent } from './paymentservice/paymentservice.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    AdminComponent,
    LogoutComponent,
    AboutComponent,
    CountryCodePipe,
    UserInfoComponent,
    GenderPipe,
    ContactUsComponent,
    ForgotpasswordComponent,
    TrialComponent,
    TripComponent,
    TripDescriptionComponent,
    EdittripComponent,
    EdituserComponent,
    RegistertripComponent,
    UserProfileComponent,
    GoogleauthComponent,
    BookingtripComponent,
    BillingComponent,
    BookingsComponent,
    FavoritetripComponent,
    MybookingsComponent,
    AddtripComponent,
    PaymentserviceComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NoopAnimationsModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    ToastrModule.forRoot({
      positionClass:'toast-top-right'
    })
    

  ],
  providers: [ToastrService],
  bootstrap: [AppComponent]
})
export class AppModule { }
