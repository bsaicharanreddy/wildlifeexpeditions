import { Component,OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { TripService } from '../trip.service';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.css']
})
export class BookingsComponent implements OnInit{

  bookings:any;

constructor(private userservice:UserService,private tripservice: TripService){



}

  ngOnInit(): void {
    this.tripservice.getAllBookedTrips().subscribe((bookings:any)=>{
      this.bookings=bookings;
    })
  }

}
