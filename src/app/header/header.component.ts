import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  loginStatus: any;
  adminLoginStatus:any;
  dropdownVisible: boolean = false;

  dropdownItems = [
    { label:"Africa",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},
    { label:"Asia",link:''},{ label:"Asia",link:''}


    
  ]

  constructor(private service: UserService) {
  }
  
  ngOnInit(){
    this.service.getLoginStatus().subscribe((loginStatusData: any) => {
      this.loginStatus = loginStatusData;
    });
    this.service.getAdminLoginStatus().subscribe((adminLoginStatusData: any) => {
      this.adminLoginStatus = adminLoginStatusData;
    });
  } 

  

  showDropdown() {
    this.dropdownVisible = true;
  }

  hideDropdown() {
    this.dropdownVisible = false;
  }
}

