import { Component, Inject,OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.css']
})
export class EdituserComponent implements OnInit {

 userObject: any;
 countries: any;
 defaultCountry: string | undefined;

  constructor(
    public dialogRef: MatDialogRef<EdituserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private service: UserService,
    private router:Router
  ) {
    this.userObject = { ...data };
  }
  ngOnInit(): void {
    this.defaultCountry = this.data.defaultCountry;
    
    this.service.getAllCountries().subscribe((countriesData: any) => {
      this.countries = countriesData;});  
  }

  updateUser(): void {
    
    this.service.updateUser(this.userObject).subscribe((userData: any) => {
      console.log(userData);
      // Optionally, you can handle the response from the server here
      // For example, you can display a success message or perform additional actions
      // based on the server response
      // You can customize this part based on your specific requirements
      // For demonstration purposes, the console.log() statement is included
      this.dialogRef.close(this.userObject);
      
    });
    
  }

  closeDialog(): void {
    this.dialogRef.close();
  }

  
}


// import { Component, Inject } from '@angular/core';
// import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
// import { UserService } from '../user.service';

// @Component({
//   selector: 'app-edituser',
//   templateUrl: './edituser.component.html',
//   styleUrls: ['./edituser.component.css']
// })
// export class EdituserComponent {

//  userObject: any;
//  countries: any;

//   constructor(
//     public dialogRef: MatDialogRef<EdituserComponent>,
//     @Inject(MAT_DIALOG_DATA) public data: any,
//     private service: UserService
//   ) {
//     this.userObject = { ...data };
//   }

//   updateUser(): void {
//     this.service.updateUser(this.userObject).subscribe((userData: any) => {
//       console.log(userData);
//       // Optionally, you can handle the response from the server here
//       // For example, you can display a success message or perform additional actions
//       // based on the server response
//       // You can customize this part based on your specific requirements
//       // For demonstration purposes, the console.log() statement is included
//       this.dialogRef.close(this.userObject);
//     });
//     this.service.getAllCountries().subscribe((countriesData: any) => {
//       this.countries = countriesData;});
    
//   }

//   closeDialog(): void {
//     this.dialogRef.close();
//   }
// }
