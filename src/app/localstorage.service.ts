import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {

  constructor() { }

  getItem(key: any): any {
    const item = localStorage.getItem(key);
    return item ? JSON.parse(item) : null;
  }

  setItem(key: any, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  removeItem(key: any): void {
    localStorage.removeItem(key);
  }

}