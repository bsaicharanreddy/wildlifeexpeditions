import { AfterViewInit, Component, ElementRef } from '@angular/core';

@Component({
  selector: 'app-trial',
  templateUrl: './trial.component.html',
  styleUrls: ['./trial.component.css']
})
export class TrialComponent implements AfterViewInit {
  constructor(private elementRef: ElementRef) {}

  ngAfterViewInit() {
    const element = document.querySelector("#ngs-exp-tripdetails-content a.trialClass1");


    if (element) {
      // Apply your desired operations to the element
      element.classList.add('custom-class');
      // ... other operations
    }
  }
}
