import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { LocalstorageService } from './localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class TripService {

  
  constructor(private http: HttpClient, private localStorageService: LocalstorageService) {
    
  }

  getAllTrips():any{
    return this.http.get('http://localhost:8080/getAllTrips');
    // console.
  }

  getTripById(tripId: number): Observable<any> {
    const url = `http://localhost:8080/getTripById/${tripId}`;
    return this.http.get(url);
  }

  getTripByBookingId(bookingTripId: number): Observable<any> {
    const url = `http://localhost:8080/getTripById/${bookingTripId}`;
    return this.http.get(url);
  }

  registerTrip(trip: any):any {
    return this.http.post('/registerTrip', trip);
  }

  updateTrip(trip: any): any {
    return this.http.put('http://localhost:8080/updateTrip', trip);
  }

  getAllBookedTrips():any{
    return this.http.get('http://localhost:8080/trips/booked');
  }

  getBookedTripsByUser(profileId:any):any{
    const url = `http://localhost:8080/users/${profileId}/bookedTrips`;
    return this.http.get(url);
  }

  bookTrip(userId:any,tripId:any){
    const url =`http://localhost:8080/users/${userId}/book-trip/${tripId}`;
    this.http.post(url,userId,tripId).subscribe((result:any)=>{
     return result;
    })
    console.log("booked trip"+userId+tripId)
  }
 

  getUserFavoriteTrips(profileId:any):any{
    const url = `http://localhost:8080/users/${profileId}/favoriteTrips`;
    return this.http.get(url);
  }

  addFavoriteTrip(tripId:any,profileId:any):any{
    const url =`http://localhost:8080/users/${profileId}/addFavorite/${tripId}`;
    this.http.post(url,tripId,profileId).subscribe((result:any)=>{
     return result;
    })
  }

  // removeFavoriteTrip(tripId: any, profileId: any): any {
  //   const url = `http://localhost:8080/users/${profileId}/removeFavorite/${tripId}`;
  //   console.log("In service");
  //   return this.http.delete(url); 
  // }

  removeFavoriteTrip(tripId: any, profileId: any): any {
    const url = `http://localhost:8080/users/${profileId}/removeFavorite/${tripId}`;
    this.http.delete(url).subscribe((result:any)=>{
      return result;
    })
  }
    // .subscribe(
    //   (response) => {
    //     console.log("Delete request successful:", response);
    //     // Handle the successful response here
    //   },
    //   (error) => {
    //     console.log("Error in delete request:", error);
    //     // Handle the error here
    //   }
    // );
 
  
  










    // getUserFavorites(userId: any): any[] {
    // const favoritesKey = this.getFavoritesKey(userId);
    // const userFavoritesString = this.localStorageService.getItem(favoritesKey);
    
    // let userFavorites = [];
    // console.log(userFavoritesString);
    // if (userFavoritesString) {
    //   try {
    //     userFavorites = JSON.parse(userFavoritesString);
    //   } catch (error) {
    //     console.error('Error parsing user favorites JSON:', error);
    //   }
    // }
    
    // return userFavorites;
  // }
  
  // getUserFavorites(userId: any): any[] {
  //   const userFavorites = JSON.parse(this.localStorageService.getItem(this.getFavoritesKey(userId))) || [];
  //   return userFavorites;
  // }


  // addUserFavorite(userId: any, trip: any): void {
    // try {
    //   const userFavorites = JSON.parse(this.localStorageService.getItem(this.getFavoritesKey(userId))) || [];
    //   userFavorites.push(trip);
    //   localStorage.setItem(this.getFavoritesKey(userId), JSON.stringify(userFavorites));
    // } catch (error) {
    //   console.error('Error adding user favorite:', error);
    // }
  // }
  
  // addUserFavorite(userId: any, trip: any): void {
  //   const userFavorites = JSON.parse(this.localStorageService.getItem(this.getFavoritesKey(userId))) || [];
  //   userFavorites.push(trip);
  //   localStorage.setItem(this.getFavoritesKey(userId), JSON.stringify(userFavorites));
  // }

  // setUserFavorites(userId: any, favorites: any[]): void {
    // const favoritesKey = this.getFavoritesKey(userId);
    // const favoritesString = JSON.stringify(favorites);
  
    // Validate JSON before setting it in localStorage
    // try {
    //   JSON.parse(favoritesString);
    //   this.localStorageService.setItem(favoritesKey, favoritesString);
    // } catch (error) {
    //   console.error('Invalid JSON format:', error);
    // }
  // }
  // setUserFavorites(userId: any, favorites: any[]): void {
  //   const favoritesKey = this.getFavoritesKey(userId);
  //   const favoritesString = JSON.stringify(favorites);
  //   this.localStorageService.setItem(favoritesKey, favoritesString);
  // }

  // removeUserFavorite(userId: any, trip: any): void {
    // const userFavorites = JSON.parse(this.localStorageService.getItem(this.getFavoritesKey(userId))) || [];
    // const updatedFavorites = userFavorites.filter((f:any) => f.tripId !== trip.tripId);
    // localStorage.setItem(this.getFavoritesKey(userId), JSON.stringify(updatedFavorites));
  // }

  // private getFavoritesKey(userId: any): any {
  //   return this.favoritesKeyPrefix + userId;
  // }

}


