import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { LogoutComponent } from './logout/logout.component';
import { AdminComponent } from './admin/admin.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TripDescriptionComponent } from './tripdescription/tripdescription.component';
import { TripComponent } from './trip/trip.component';
import { GoogleauthComponent } from './googleauth/googleauth.component';
import { BookingtripComponent } from './bookingtrip/bookingtrip.component';
import { BillingComponent } from './billing/billing.component';
import { FavoritetripComponent } from './favoritetrip/favoritetrip.component';
import { MybookingsComponent } from './mybookings/mybookings.component';
import { BookingsComponent } from './bookings/bookings.component';
import { UserProfileComponent } from './profile/profile.component';
import { AddtripComponent } from './addtrip/addtrip.component';
import { PaymentserviceComponent } from './paymentservice/paymentservice.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';


const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "login", component: LoginComponent },
  { path: "logout", component: LogoutComponent },
  { path: "register", component: RegisterComponent },
  { path: "home", component: HomeComponent },
  { path: "about", component: AboutComponent },
  { path: "logout", component: LogoutComponent },
  { path: "admin", component: AdminComponent },
  { path: "userInfo", component: UserInfoComponent },
  { path: 'contactUs', component: ContactUsComponent },
  { path: 'trip', component: TripComponent },
  { path: 'bookings', component:BookingsComponent },
  { path: 'profile', component: UserProfileComponent },
  { path: 'tripdescription/:tripId', component: TripDescriptionComponent },
  { path: 'bookingtrip/:bookingTripId', component: BookingtripComponent },
  { path: 'googleauth', component: GoogleauthComponent },
  { path: 'billing/:billingId', component: BillingComponent },
  { path: 'favouritetrip', component: FavoritetripComponent },
  { path: 'mybookings', component: MybookingsComponent },
  { path: 'addtrip'  , component: AddtripComponent},
  { path: 'payment'  , component:PaymentserviceComponent},
  { path: "forgotpassword"  , component:ForgotpasswordComponent}

  
  

  // { path: 'bookingtrip',          component: BookingtripComponent },


  // { path: 'tripdesc',          component: TripDescriptionComponent }

  // { path: 'trip/:tripId', component: TripComponent },
  
  // { path: 'trip/:tripId', component: TripComponent },
  

 
  // { path: 'home/bookingtrip', component: BookingtripComponent }

  // { path: 'trip/:tripId', component: TripComponent },
  // { path: 'trip/:tripId/description', component: TripDescriptionComponent }

  // { path: 'tripdesc/:tripId',      component: TripDescriptionComponent }

  // { path: 'trips', component: TripComponent },
  // { path: 'tripdescription/:tripId', component: TripDescriptionComponent },
  // { path: '', redirectTo: '/trips', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
