import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'] 
})

export class RegisterComponent implements OnInit {
  
  countries: any;

  emailIds:any;

    // countryCode:string='';
  user: any;
  regForm:any;


  userName: string = '';
  usernameTouched: boolean = false;
  usernameValidations: { [key: string]: boolean } = {
  capital: false,
  length: false,
  maxlength: false,
  special: false
};

mobileNumber:any;
  isMobileNumberValid: boolean = true;
  isMobileNumberValidCharacter: boolean = true;
mobileNumberTouched: boolean = false;

emailId: string = '';
emailTouched: boolean = false;
emailValidations: { [key: string]: boolean } = {
  format: false
};


  password: string = '';
  passwordTouched: boolean = false;
  passwordValidations: { [key: string]: boolean } = {
    length: false,
    uppercase: false,
    lowercase: false,
    digit: false,
    special: false
  };

  confirmPassword: string = '';
  confirmPasswordTouched: boolean = false;
isPasswordMismatch: boolean = false;
passwordMatch: boolean = false;





  
  //Implementing Dependency Injection for EmpService using Constructor
  constructor(private service:UserService,private router:Router,private toastr:ToastrService) {

    this.user = {userId:'',userName:'',gender:'', country:'', mobileNumber:'',emailId:'',
     password:''};

  }
  
  ngOnInit(){

    this.service.getAllCountries().subscribe((data: any) => {
      this.countries = data;
      console.log(data);
      this.service.getAllEmailIds().subscribe((emails: any) => {
         this.emailIds = emails;
        });
 
    });
  }

  //email already exists
  // Assume you have the emailId and the flag indicating whether it already exists
 // Flag indicating whether the emailId already exists

// Function to check if the emailId already exists
checkEmailIdExists(emailId: string): boolean {
  return this.emailIds.includes(emailId);
}


//User name valdation

isUsernameValid(condition: string): boolean {
  return this.usernameValidations[condition];
}

clearUsernameValidation(): void {
  this.usernameTouched = true;
}

updateUsernameValidation(): void {
  this.usernameValidations['capital'] = /^[A-Z]/.test(this.userName);
  this.usernameValidations['length'] = this.userName.length >= 5;
  this.usernameValidations['maxlength'] = this.userName.length <= 30;
  this.usernameValidations['special'] = /^[A-Za-z0-9]*$/.test(this.userName);
}

 //mobileNumber validation


validateMobileNumber(): void {
  const isValidLength = this.mobileNumber?.length === 10;
  const isValidCharacter = /^[0-9]+$/.test(this.mobileNumber);
  this.isMobileNumberValid = isValidLength;
  this.isMobileNumberValidCharacter = isValidCharacter;
}

clearMobileNumberValidation(): void {
  this.mobileNumberTouched = true;
}



// Email Validation
isEmailValid(condition: string): boolean {
  return this.emailValidations[condition];
}

clearEmailValidation(): void {
  this.emailTouched = true;
}

updateEmailValidation(): void {
  this.emailValidations['format'] = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(this.emailId);
}




//Password Validation
isPasswordValid(condition: string): boolean {
  return this.passwordValidations[condition];
}

clearPasswordValidation(): void {
  this.passwordTouched = true;
}

updatePasswordValidation(): void {
  this.passwordValidations['length'] = this.password.length >= 8;
  this.passwordValidations['uppercase'] = /[A-Z]/.test(this.password);
  this.passwordValidations['lowercase'] = /[a-z]/.test(this.password);
  this.passwordValidations['digit'] = /\d/.test(this.password);
  this.passwordValidations['special'] = /\W/.test(this.password);
}

//confirm password validation


allPasswordConditionsSatisfied(): boolean {
  return (
    this.isPasswordValid('length') &&
    this.isPasswordValid('uppercase') &&
    this.isPasswordValid('lowercase') &&
    this.isPasswordValid('digit') &&
    this.isPasswordValid('special')
  );
}

confirmPasswordMismatch: boolean = false;

checkPasswordMatch(): void {
  this.confirmPasswordMismatch = false;
  if (this.password.length === this.confirmPassword.length) {
    for (let i = 0; i < this.password.length; i++) {
      if (this.password[i] !== this.confirmPassword[i]) {
        this.confirmPasswordMismatch = true;
        break;
      }
    }
  } else {
    this.confirmPasswordMismatch = true;
  }
  this.passwordMatch = !this.confirmPasswordMismatch;
}

clearConfirmPasswordValidation(): void {
  this.confirmPasswordTouched = true;
}




// checkPasswordMatch(): void {
//   this.isPasswordMismatch = this.password !== this.confirmPassword;
// }



// Submit button validation
allConditionsSatisfied(): boolean {
  return this.isUsernameValid('capital') && this.isUsernameValid('length') && this.isUsernameValid('maxlength') &&
    this.isUsernameValid('special') && this.isMobileNumberValid && this.isMobileNumberValidCharacter &&
    this.isEmailValid('format') && this.isPasswordValid('length') && this.isPasswordValid('uppercase') &&
    this.isPasswordValid('lowercase') && this.isPasswordValid('digit') && this.isPasswordValid('special') &&
    this.passwordMatch&&this.allPasswordConditionsSatisfied();
}


//registration
  userRegistration(regFrom: any) {
    console.log(regFrom);

    this.user.userName = regFrom.userName;
    this.user.gender = regFrom.gender;
    this.user.country = regFrom.country;
    this.user.mobileNumber = regFrom.mobileNumber;
    this.user.emailId = regFrom.emailId;
    this.user.password = regFrom.password;

    console.log(this.user);
    this.service.registerUser(this.user).subscribe((data: any) => {
      console.log(data);
      
    });
    this.toastr.success('registration successful !');
    this.router.navigate(['login']);
  }

}