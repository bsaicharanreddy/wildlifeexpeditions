import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'countryCode'
})
export class CountryCodePipe implements PipeTransform {

  transform(prefix: string,suffix: string,): string {
    return prefix+suffix;
  }

}
