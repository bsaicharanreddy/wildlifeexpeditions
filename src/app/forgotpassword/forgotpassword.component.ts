import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit{

  emailExists: any;
  emailId: string = '';
emailTouched: boolean = false;
emailValidations: { [key: string]: boolean } = {
  format: false
};

password: string = '';
passwordTouched: boolean = false;
passwordValidations: { [key: string]: boolean } = {
  length: false,
  uppercase: false,
  lowercase: false,
  digit: false,
  special: false
};

confirmPassword: string = '';
confirmPasswordTouched: boolean = false;
isPasswordMismatch: boolean = false;
passwordMatch: boolean = false;

  // enteredOTP: string;
  // otp: string;
  otpEnabled: boolean = false;

  checkEmailIdExists(emailId: string): void {
    this.emailExists = this.checkEmailExists(emailId);
    this.otpEnabled = this.emailExists;
  }

  emailIds:any;

  constructor(private service:UserService,private router:Router){

  }

  ngOnInit(): void {
    this.service.getAllEmailIds().subscribe((emails: any) => {
      this.emailIds = emails;
     });
  }

  // email validation 
  checkEmailExists(emailId: string): boolean {
    return this.emailIds.includes(emailId);
  }
  isEmailValid(condition: string): boolean {
    return this.emailValidations[condition];
  }
  
  clearEmailValidation(): void {
    this.emailTouched = true;
  }
  
  updateEmailValidation(): void {
    this.emailValidations['format'] = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/.test(this.emailId);
  }

  //sending otp
 
  isPasswordValid(condition: string): boolean {
    return this.passwordValidations[condition];
  }
  
  clearPasswordValidation(): void {
    this.passwordTouched = true;
  }
  
  updatePasswordValidation(): void {
    this.passwordValidations['length'] = this.password.length >= 8;
    this.passwordValidations['uppercase'] = /[A-Z]/.test(this.password);
    this.passwordValidations['lowercase'] = /[a-z]/.test(this.password);
    this.passwordValidations['digit'] = /\d/.test(this.password);
    this.passwordValidations['special'] = /\W/.test(this.password);
  }
  
  //confirm password validation
  
  
  allPasswordConditionsSatisfied(): boolean {
    return (
      this.isPasswordValid('length') &&
      this.isPasswordValid('uppercase') &&
      this.isPasswordValid('lowercase') &&
      this.isPasswordValid('digit') &&
      this.isPasswordValid('special')
    );
  }
  
  confirmPasswordMismatch: boolean = false;
  
  checkPasswordMatch(): void {
    this.confirmPasswordMismatch = false;
    if (this.password.length === this.confirmPassword.length) {
      for (let i = 0; i < this.password.length; i++) {
        if (this.password[i] !== this.confirmPassword[i]) {
          this.confirmPasswordMismatch = true;
          break;
        }
      }
    } else {
      this.confirmPasswordMismatch = true;
    }
    this.passwordMatch = !this.confirmPasswordMismatch;
  }
  
  clearConfirmPasswordValidation(): void {
    this.confirmPasswordTouched = true;
  }

  sendOTP(ForgotPasswordForm:any){
    // Call your API endpoint to send the OTP to the email address
    this.service.sendOtp(ForgotPasswordForm.emailId);
    console.log("otp sent")
    // .subscribe(
    //   (response: any) => {
    //     console.log(response);
    //   },
    //   (error: any) => {
    //     console.error('Error occurred while sending OTP:', error);
    //     // Handle error response
    //   }
    // );
  }


  verifyOtp(emailId:any,otp:any){
    this.service.verifyOtp(emailId,otp);
    console.log("otp verified")
    // .subscribe((result:any)=>{
    //   return result;
    //   // console.log("verifyOtp"+result);
    // })
    // console.log("verify otp"+emailId);
    // console.log("verify otp"+otp);
    

  }
  

  changePassword(forgotPasswordForm:any){

    this.service.changePassword(forgotPasswordForm.emailId,forgotPasswordForm.password);
    console.log("changed password")
    // .subscribe((result:any)=>{
    //   return result;
    // })
  }

}
