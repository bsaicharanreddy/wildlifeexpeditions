import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { TripService } from '../trip.service';

@Component({
  selector: 'app-bookingtrip',
  templateUrl: './bookingtrip.component.html',
  styleUrls: ['./bookingtrip.component.css']
})export class BookingtripComponent implements OnInit{

  tripId:any;
  trip:any;
  bookedTrip:any

  constructor(private tripservice:TripService, private route: ActivatedRoute,private userservice:UserService, private router:Router){
    this.bookedTrip = {  
      tripTitle: '',
      continent: '',
      country: '',
      tripType: '',
      price: '',
      activityLevel: '',
      serviceLevel: '',
      tripDuration: '',
      tripOverview: '',
      heroImage:''

    }
  }

  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.tripId = params['bookingTripId'];
    });

      this.tripservice.getTripByBookingId(this.tripId).subscribe((trip: any) => {
        this.trip = trip;
        this.bookedTrip= {  
          tripId:this.trip.tripId,
          tripTitle: this.trip.tripTitle,
          continent: this.trip.continent,
          country: this.trip.country,
          tripType: this.trip.tripType,
          price: this.trip.price,
          heroImage:this.trip.heroImage,
          activityLevel: this.trip.activityLevel,
          serviceLevel: this.trip.serviceLevel,
          tripDuration: this.trip.tripDuration,
          tripOverview: this.trip.tripOverview,
          itinerary: this.trip.itinerary,
          whatToExpect: this.trip.whatToExpect,
          accommodation:this.trip.accommodation,
          expertTeam: this.trip.expertTeam,
          transportation: this.trip.transportation,
          whatIncluded: this.trip.whatIncluded,
          whatNotIncluded: this.trip.whatNotIncluded
        }
        
        console.log(this.bookedTrip.heroImage)
      }, (error: any) => {
        console.error(error);
      });
    }

    billing(billingId:any) {
      const userId=this.userservice.getLoggedProfileId();
      this.userservice.setBookedTripPrice(this.bookedTrip.price);
      console.log(billingId)
      this.tripservice.bookTrip(userId,billingId);
      this.router.navigate(['/billing', billingId]);
    }



}
