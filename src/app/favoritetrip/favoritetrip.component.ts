import { Component } from '@angular/core';
import { TripService } from '../trip.service';
import { UserService } from '../user.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-favoritetrip',
  templateUrl: './favoritetrip.component.html',
  styleUrls: ['./favoritetrip.component.css']
})
export class FavoritetripComponent {

  favoriteTrips: any;
  profileId:any;

  loginStatus: any;

  result:any;

  constructor(private tripService: TripService, private service:UserService,
    private router:Router, private toastr:ToastrService) { }

  ngOnInit(): void {

    this.profileId=this.service.getLoggedProfileId();
    this.tripService.getUserFavoriteTrips(this.profileId).subscribe((favoriteTrips:any)=>{
      this.favoriteTrips=favoriteTrips;
      console.log(this.favoriteTrips);
    })
    this.loginStatus=this.service.getUserLoggedStatus()
  }
    removeFavorite(tripId:any){
      this.tripService.removeFavoriteTrip(tripId, this.profileId);
      console.log(tripId);
      console.log(this.profileId);
      this.toastr.error('removed from Favorites');
      this.router.navigate(['favouritetrip']);
    }

    bookTrip(bookingTripId: any) {
      // const profileId=this.service.getProfileId();
      // this.service.addBookedTrip(profileId,bookingTripId);
      // if(this.loginStatus){
        this.router.navigate(['/bookingtrip', bookingTripId]);
      // }else{
        // this.router.navigate(['/login']);
      // }
      
    }

}

