import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserService } from '../user.service';
import { TripService } from '../trip.service';

@Component({
  selector: 'app-edittrip',
  templateUrl: './edittrip.component.html',
  styleUrls: ['./edittrip.component.css'],
})
export class EdittripComponent {
  tripObject: any;

  constructor(
    public dialogRef: MatDialogRef<EdittripComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private tripservice: TripService
  ) {
    this.tripObject = { ...data };
  }

  updateTrip(): void {
    console.log('Updating trip:', this.tripObject);
    this.tripservice.updateTrip(this.tripObject).subscribe((tripData: any) => {
      console.log(tripData);
      console.log('Update response:', tripData);
      this.dialogRef.close(this.tripObject);
    });
  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}




// import { Component, Inject } from '@angular/core';
// import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
// import { UserService } from '../user.service';

// @Component({
//   selector: 'app-edittrip',
//   templateUrl: './edittrip.component.html',
//   styleUrls: ['./edittrip.component.css']
// })
// export class EdittripComponent {

//   tripObject:any;

//   constructor(
//     public dialogRef: MatDialogRef<EdittripComponent>,
//     @Inject(MAT_DIALOG_DATA) public data: any,private service:UserService
//   ) { 
//     this.tripObject = { data };
//   }

//   updateTrip(): void {
//     this.service.updateTrip(this.tripObject).subscribe((tripData: any) => {
//       console.log(tripData);
//       // Optionally, you can handle the response from the server here
//       // For example, you can display a success message or perform additional actions
//       // based on the server response
//       // You can customize this part based on your specific requirements
//       // For demonstration purposes, the console.log() statement is included
//     });
//   }
  

//   closeDialog(): void {
//     this.dialogRef.close(this.data);
//   }

// }