// user-profile.component.ts

import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class UserProfileComponent implements OnInit {
  
  profile:any;
  profileId:any;
  countries:any;

  constructor(private service: UserService) {
    this.profile = {
      name: '',
      mobileNumber: '',
      gender: '',
      country: '',
      emailId: '',
      password: ''
    };
   }

  ngOnInit(): void {
    this.profileId=this.service.getLoggedProfileId();
    this.service.getUserByUserId().subscribe((profile:any)=>{
      this.profile=profile;
      console.log(this.profile); 
    });
    this.service.getAllCountries().subscribe((countries:any)=>{
      this.countries=countries;
    })
    // this.service.getUserByUserId().subscribe((profile:any)=>{
    //   this.profile=profile;
    //   console.log(this.profile);
    // })
    
  }
  
    updateUser(): void {
      this.service.updateUser(this.profile).subscribe((updatedUser: any) => {
        console.log('User updated successfully:', updatedUser);
      });
    }
  }
