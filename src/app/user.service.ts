import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject, switchMap } from 'rxjs';
import { LocalstorageService } from './localstorage.service';



function _window():any{
  return window;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  id:any;

  private tripId: any;
  private tripIdSubject: Subject<any> = new Subject<any>();

  private bookingTripId: any;
  private bookingTripIdSubject: Subject<any> = new Subject<any>();

  profileId: any;
  loggedUserProfileId: Subject<any>

  bookedTripPrice: any;
  bookedTripBookedTripPrice: Subject<any>

  isUserLogged: boolean;
  loginStatus: Subject<any>

  isAdminLogged: boolean;
  adminLoginStatus: Subject<any>



  //Implementing Dependency Injection for HttpClient using Constructor
  constructor(private http: HttpClient, private localStorageService: LocalstorageService) {
    this.profileId = 0;
    this.loggedUserProfileId = new Subject();

    this.bookedTripPrice = 0;
    this.bookedTripBookedTripPrice= new Subject();

    this.isUserLogged = false;
    this.loginStatus = new Subject();

    this.isAdminLogged = false;
    this.adminLoginStatus = new Subject();
  }

  getProfileId():any{
    return this.loggedUserProfileId.asObservable();
  }

  getBookedTripPrice():any{
    return this.bookedTripBookedTripPrice.asObservable();
  }

  getLoginStatus(): any {
    return this.loginStatus.asObservable();
  }

  getAdminLoginStatus(): any {
    return this.adminLoginStatus.asObservable();
  }

  setUserLoggedIn() {
    this.isUserLogged = true;
    this.loginStatus.next(true);
  }

  setAdminLoggedIn() {
    this.isAdminLogged = true;
    this.adminLoginStatus.next(true);
  }

  setProfileId(profileId: any): void {
    this.profileId = profileId;
    this.loggedUserProfileId.next(profileId);
  }

  setBookedTripPrice(bookedTripPrice: any): void {
    this.bookedTripPrice = bookedTripPrice;
    this.bookedTripBookedTripPrice.next(bookedTripPrice);
  }

  getUserLoggedStatus(): boolean {
    return this.isUserLogged;
  }

  getAdminLoggedStatus(): boolean {
    return this.isAdminLogged;
  }

  getLoggedProfileId(): any {
    return this.profileId;
  }

  getbookedTripBookedTripPrice(): any {
    return this.bookedTripPrice;
  }

  setLogoutUserId() {
    this.profileId = 0;
    this.loggedUserProfileId.next(0);
  }

  setUserLogout() {
    this.isUserLogged = false;
    this.loginStatus.next(false);
  }

  setAdminLogout() {
    this.isAdminLogged = false;
    this.adminLoginStatus.next(false);
  }

  getAllCountries(): any {
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllUsers(): any {
    return this.http.get('/getAllUsers');
  }

  getUserById(userId: any): any {
    return this.http.get('/getUserById/' + userId);
  }

  getUser(loginForm: any): any {
    return this.http.get('/login/' + loginForm.emailId + "/" + loginForm.password).toPromise();
  }

  getUserByUserId(): any {
    const url = `http://localhost:8080/getUserById/${this.profileId}`;
    return this.http.get(url);
  }

  getUserByEmailId(emailId:any):any{
    return this.http.get('/getUserByEmailId/' + emailId);
  }

  deleteUser(userId: any): any {
    return this.http.delete('/deleteUser/' + userId)
  }

  registerUser(user: any) {
    return this.http.post('/registerUser', user);
  }

  updateUser(user: any): any {
    return this.http.put('/updateUser', user);
  }

  getAllEmailIds(): any {
    return this.http.get('`http://localhost:8080/getAllEmailIds');
  }

  sendOtp(emailId: any): any {
    console.log(emailId)
    return this.http.post('http://localhost:8080/resetPassword/', emailId).subscribe((result:any)=>{
      return result;
      
    })
    
  }

  verifyOtp(emailId:any,otp:any):any{
    this.http.post('http://localhost:8080/verifyOTP/{emailId}/{otp}',{emailId,otp}).subscribe((result:any)=>{
      return result;
    })
  }

  changePassword(emailId:any,password:any):any{
    return this.http.post('http://localhost:8080/changePassword/{emailId}/{password}',{emailId,password}).subscribe((result:any)=>{
      return result;
    })
  }

  get nativeWindow():any{
    return _window();
  }


  // verifyOtp(emailId:any,otp:any):any{
  //   const url=`http://localhost:8080/verifyOTP/${emailId}/${otp}`;
  //   return this.http.post(url);
  // }

  // verifyOtp(emailId: any, otp: any): any {
  //   const url = `http://localhost:8080/verifyOTP/${emailId}/${otp}`;
  //   const body = {}; // Provide the request body if required
  
  //   return this.http.post(url, body);
  // }

  // verifyOtp(emailId: any, otp: any): any {
  //   const url = 'http://localhost:8080/verifyOTP';
  //   const params = new HttpParams()
  //     .set('emailId', emailId)
  //     .set('otp', otp);
  
  //   return this.http.post(url, null, { params });
  // }

  // getAllTrips():any{
  //   return this.http.get('http://localhost:8080/getAllTrips');
  // }

  // getTripsByCountry(country:any){
  //   return this.http.get('/getTripsByCountry',country);
  // }

  // getTripById(tripId: number): Observable<any> {
  //   const url = `http://localhost:8080/getTripById/${tripId}`;
  //   return this.http.get(url);
  // }
  // // getTripById(tripId:any):any{
  // //   return this.http.get('/getTripById',tripId)
  // // }

  // setTripId(tripId: any): void {
  //   this.tripId = tripId;
  //   this.tripIdSubject.next(tripId);
  // }

  // getTripId(): Observable<any> {
  //   return this.tripIdSubject.asObservable();
  // }

  // setBookingTripId(bookingTripId: any): void {
  //   this.bookingTripId = bookingTripId;
  //   this.bookingTripIdSubject.next(bookingTripId);
  // }

  // getBookedTrip(): Observable<any> {
  //   return this.bookingTripIdSubject.asObservable();
  // }

  // getTripByBookingId(bookingTripId: number): Observable<any> {
  //   const url = `http://localhost:8080/getTripById/${bookingTripId}`;
  //   return this.http.get(url);
  // }

  // // getTripDesc():any{
  // //   return this.http.get('http://localhost:8080/getTripById',this.tripId);
  // // } 

  // updateTrip(trip: any): any {
  //   return this.http.put('http://localhost:8080/updateTrip', trip);
  // }



 




  //   updateEmployee(emp: any): any {
  //     return this.http.put('/updateEmployee', emp);
  // } deleteEmployee(empId: any): any {
  //   return this.http.delete('/deleteEmployee/' + empId)
  // }getDepartments(): any {
  //   return this.http.get('http://localhost:8080/getAllDepartments');
  // }getAllEmployees(): any {
  //   return this.http.get('http://localhost:8080/getAllEmployees');
  // }

}




