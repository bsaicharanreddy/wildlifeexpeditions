import { Component, OnInit } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { EdittripComponent } from '../edittrip/edittrip.component';
import { TripService } from '../trip.service';

declare const $: any;

  
@Component({
  selector: 'app-trip',
  templateUrl: './trip.component.html',
  styleUrls: ['./trip.component.css'],
})
export class TripComponent implements OnInit, AfterViewInit {
  
  trips: any[]=[];
  currentTrips: any[] = [];
  selectedCountries: string[]=[];
  selectedContinents:string[]=[];
  selectedDuration: string='';
  selectedPrice: string='';
  priceSort: string = 'asc';
  durationSort: string = 'asc';
  tripDesc:any;


  selectCountries=[
    {value:'',label:'All'},
    {value:'INDIA',label:'INDIA'},
    {value:'BORNEO',label:'BORNEO'},
    {value:'CAMBODIA',label:'CAMBODIA'},
    {value:'MONGOLIA',label:'MONGOLIA'},
    {value:'JAPAN',label:'JAPAN'},
    {value:'TANZANIA',label:'TANZANIA'},
    {value:'KENYA',label:'KENYA'},
    {value:'TANZANIA',label:'TANZANIA'},
    {value:'MADAGASCAR',label:'MADAGASCAR'},
    {value:'BOTSWANA',label:'BOTSWANA'},
    {value:'NAMIBIA',label:'NAMIBIA'},
    {value:'SOUTH AFRICA',label:'SOUTH AFRICA'},
    {value:'UGANDA',label:'UGANDA'},
    {value:'ZAMBIA',label:'ZAMBIA'},
    {value:'ZIMBABWE',label:'ZIMBABWE'},
    {value:'AUSTRALIA',label:'AUSTRALIA'},
    {value:'NEW ZEALAND',label:'NEW ZEALAND'},
    {value:'PALAU',label:'PALAU'}
  ]
  selectContinents=[
    {value:'',label:'All'},
    {value:'ASIA',label:'ASIA'},
    {value:'AFRICA',label:'AFRICA'},
    {value:' Australia and the Pacific',label:' Australia and the Pacific'}
  ]
  priceLevel=[
    {value:'',label:'any'},
    {value:'0-1000',label:'$ 0-1000'},
    {value:'1001-10000',label:'$1001-$10000'},
    {value:'1001-10000',label:'$10001-$50000'},
    {value:'50001-100000',label:'$50001-$100000'},
    {value:'100001-1000000',label:'$100001-$1000000'},
    {value:'',label:''}
  ]

  loginStatus: any;
  adminLoginStatus:any;


  constructor(
    private tripservice: TripService,
    private router: Router,
    private dialog: MatDialog
  ) { }
  ngAfterViewInit(): void {
    // $('#countryFilter').select2();
  }

  ngOnInit(): void {

    this.getAllTrips();
    
  }

  getAllTrips(): void {
    this.tripservice.getAllTrips().subscribe((trips:any) => {
        this.trips = trips;
        this.applyFilters();
      });
  }

  applyFilters(): void {
    let currentTrips = this.trips;

    if (this.selectedContinents.length > 0) {
      currentTrips = currentTrips.filter(trip => this.selectedContinents.includes(trip.continent));
    }

    if (this.selectedCountries.length > 0) {
      currentTrips = currentTrips.filter(trip => this.selectedCountries.includes(trip.country));
    }

    if (this.selectedDuration) {
      const [minDuration, maxDuration] = this.selectedDuration.split('-');
      currentTrips = currentTrips.filter(trip => trip.tripDuration >= +minDuration && trip.tripDuration <= +maxDuration);
    }
    if (this.selectedPrice) {
      const [minPrice, maxPrice] = this.selectedPrice.split('-');
      currentTrips = currentTrips.filter(trip => trip.price >= +minPrice && trip.price <= +maxPrice);
    }
    if (this.priceSort === 'asc') {
      currentTrips.sort((a, b) => a.price - b.price);
    } else if (this.priceSort === 'desc') {
      currentTrips.sort((a, b) => b.price - a.price);
    }

    // Sort by duration
    if (this.durationSort === 'asc') {
      currentTrips.sort((a, b) => a.tripDuration - b.tripDuration);
    } else if (this.durationSort === 'desc') {
      currentTrips.sort((a, b) => b.tripDuration - a.tripDuration);
    }
    else{
      this.currentTrips=this.trips;
    }

    this.currentTrips = currentTrips;
  }

//  exploreTrip(tripId: any): void {
//       this.router.navigate(['/tripdesc', tripId]);
//     }
// exploreTrip(tripId:any): void {
//   // Navigate to the trip description page with the tripId as a route parameter
//   this.service.setTripId(tripId);
// }
exploreTrip(tripId:any): void {
  // Navigate to the trip description page with the tripId as a route parameter
  this.router.navigate(['/tripdescription', tripId]);
}
// exploreTrip(tripId: string) {
//   // Open the trip description in a new page
//   window.open(`/tripdescription/${tripId}`, '_blank');
// }


  editTrip(trip: any) {
    const dialogRef = this.dialog.open(EdittripComponent, {
      width: '500px',
      data: { ...trip },
    });

    dialogRef.afterClosed().subscribe((updatedTrip: any) => {
      if (updatedTrip) {
        // Find the index of the updated trip in the trips array
        const index = this.trips.findIndex((t: any) => t.tripId === updatedTrip.tripId);

        if (index !== -1) {
          // Update the trip object in the array with the updated data
          this.trips[index] = { ...updatedTrip };
        }
      }
    });
  }


}


// import { Component, OnInit } from '@angular/core';
// import { UserService } from '../user.service';
// import { trigger } from '@angular/animations';
// import { transition, animate, style } from '@angular/animations';
// import { Router } from '@angular/router';
// import { MatDialog } from '@angular/material/dialog';
// import { EdittripComponent } from '../edittrip/edittrip.component';


// @Component({
//   selector: 'app-trip',
//   templateUrl: './trip.component.html',
//   styleUrls: ['./trip.component.css'],
//   animations: [
//     trigger('slideInLeft', [
//       transition(':enter', [
//         style({ transform: 'translateX(-100%)' }),
//         animate('500ms', style({ transform: 'translateX(0)' }))
//       ])
//     ])
//   ]
// })
// export class TripComponent implements OnInit {

//   trips: any;
//   adminLogin:any;
//   userLogin:any;

//   constructor(private service: UserService, private router: Router,private dialog: MatDialog) { 
//   }

//   ngOnInit(): void {

//     this.service.getAllTrips().subscribe((tripData: any) => {
//       this.trips = tripData;
//     })
//     this.service.getLoginStatus().subscribe((loginStatusData: any) => {
//       this.userLogin = loginStatusData;
//     });
//     this.service.getAdminLoginStatus().subscribe((adminLoginStatusData: any) => {
//       this.adminLogin = adminLoginStatusData;
//     });
//   }

//   tripDescription(tripId: any) {

//     this.service.setTripId(tripId);
//     this.router.navigate(['tripdesc']);

//   }

//     editTrip(trip: any) {
//     const dialogRef = this.dialog.open(EdittripComponent  , {
//       width: '500px',
//       data: { ...trip }
//     });

//     dialogRef.afterClosed().subscribe((updatedTrip: any) => {
//       if (updatedTrip) {
//         // Perform update logic with the updated trip data
//         console.log(updatedTrip);
//       }
//     });
//   }
// }






